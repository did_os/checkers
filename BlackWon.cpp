//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "BlackWon.h"
#include "Game.h"
#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TBlackWin *BlackWin;
//---------------------------------------------------------------------------
__fastcall TBlackWin::TBlackWin(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TBlackWin::OKClick(TObject *Sender)
{
Main_Menu_Form->Show();
this->Close();
Game_Form->Close();
}
//---------------------------------------------------------------------------
