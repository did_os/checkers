//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
//---------------------------------------------------------------------------
class TMain_Menu_Form : public TForm
{
__published:	// IDE-managed Components
	TPanel *Main_Menu;
	TLabel *Game_Name;
	TPanel *Buttons_Pannel;
	TButton *New_Game;
	TButton *Exit_Game;
	TButton *Game_Rules;
	void __fastcall Game_RulesClick(TObject *Sender);
	void __fastcall Exit_GameClick(TObject *Sender);
	void __fastcall New_GameClick(TObject *Sender);
private:	// User declarations
public:	  bool info;	// User declarations
	__fastcall TMain_Menu_Form(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMain_Menu_Form *Main_Menu_Form;
//---------------------------------------------------------------------------
#endif
