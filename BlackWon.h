//---------------------------------------------------------------------------

#ifndef BlackWonH
#define BlackWonH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TBlackWin : public TForm
{
__published:	// IDE-managed Components
	TPanel *BlackWon;
	TButton *OK;
	void __fastcall OKClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TBlackWin(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TBlackWin *BlackWin;
//---------------------------------------------------------------------------
#endif
