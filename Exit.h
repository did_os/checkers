//---------------------------------------------------------------------------

#ifndef ExitH
#define ExitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TExit_Form : public TForm
{
__published:	// IDE-managed Components
	TPanel *Exit_Panel;
	TLabel *Exit_Label;
	TButton *Yes_Button;
	TButton *No_Button;
	void __fastcall Yes_ButtonClick(TObject *Sender);
	void __fastcall No_ButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TExit_Form(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TExit_Form *Exit_Form;
//---------------------------------------------------------------------------
#endif
