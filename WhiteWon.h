//---------------------------------------------------------------------------

#ifndef WhiteWonH
#define WhiteWonH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TWhiteWin : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TButton *OK;
	void __fastcall OKClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TWhiteWin(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TWhiteWin *WhiteWin;
//---------------------------------------------------------------------------
#endif
