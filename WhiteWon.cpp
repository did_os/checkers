//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "WhiteWon.h"
#include "Game.h"
#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TWhiteWin *WhiteWin;
//---------------------------------------------------------------------------
__fastcall TWhiteWin::TWhiteWin(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TWhiteWin::OKClick(TObject *Sender)
{
Main_Menu_Form->Show();
this->Close();
Game_Form->Close();
}
//---------------------------------------------------------------------------
