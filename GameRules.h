//---------------------------------------------------------------------------

#ifndef GameRulesH
#define GameRulesH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TRules_Form : public TForm
{
__published:	// IDE-managed Components
	TPanel *Rules_Text_Panel;
	TRichEdit *Rules_Text;
private:	// User declarations
public:		// User declarations
	__fastcall TRules_Form(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TRules_Form *Rules_Form;
//---------------------------------------------------------------------------
#endif
