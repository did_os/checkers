object Rules_Form: TRules_Form
  AlignWithMargins = True
  Left = 488
  Top = 110
  Align = alCustom
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1055#1088#1072#1074#1080#1083#1072' '#1048#1075#1088#1099
  ClientHeight = 542
  ClientWidth = 498
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  GlassFrame.Enabled = True
  OldCreateOrder = False
  Position = poDefault
  PixelsPerInch = 96
  TextHeight = 13
  object Rules_Text_Panel: TPanel
    Left = 0
    Top = 0
    Width = 498
    Height = 542
    Align = alClient
    BevelInner = bvSpace
    BevelKind = bkTile
    BevelOuter = bvSpace
    BorderStyle = bsSingle
    TabOrder = 0
    object Rules_Text: TRichEdit
      AlignWithMargins = True
      Left = 5
      Top = 5
      Width = 480
      Height = 524
      Align = alClient
      Alignment = taCenter
      BevelInner = bvSpace
      BevelKind = bkFlat
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clCream
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Lines.Strings = (
        'Rules_Text')
      ParentFont = False
      PlainText = True
      ReadOnly = True
      TabOrder = 0
    end
  end
end
