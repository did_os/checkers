//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------






#include <Vcl.Styles.hpp>
#include <Vcl.Themes.hpp>



































USEFORM("Main.cpp", Main_Menu_Form);
USEFORM("Exit.cpp", Exit_Form);
USEFORM("GameRules.cpp", Rules_Form);
USEFORM("Game.cpp", Game_Form);
USEFORM("BlackWon.cpp", BlackWin);
USEFORM("WhiteWon.cpp", WhiteWin);
//---------------------------------------------------------------------------
WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
	try
	{
		Application->Initialize();
		Application->MainFormOnTaskBar = true;
		TStyleManager::TrySetStyle("Golden Graphite");
		Application->CreateForm(__classid(TMain_Menu_Form), &Main_Menu_Form);
		Application->CreateForm(__classid(TRules_Form), &Rules_Form);
		Application->CreateForm(__classid(TExit_Form), &Exit_Form);
		Application->CreateForm(__classid(TGame_Form), &Game_Form);
		Application->CreateForm(__classid(TBlackWin), &BlackWin);
		Application->CreateForm(__classid(TWhiteWin), &WhiteWin);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------
