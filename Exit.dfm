object Exit_Form: TExit_Form
  Left = 488
  Top = 329
  Align = alCustom
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = #1042#1099#1093#1086#1076
  ClientHeight = 211
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  GlassFrame.Enabled = True
  OldCreateOrder = False
  Position = poDesigned
  ExplicitWidth = 320
  PixelsPerInch = 96
  TextHeight = 13
  object Exit_Panel: TPanel
    Left = 0
    Top = 0
    Width = 457
    Height = 211
    Align = alClient
    BevelInner = bvSpace
    BevelKind = bkSoft
    TabOrder = 0
    object Exit_Label: TLabel
      AlignWithMargins = True
      Left = 5
      Top = 5
      Width = 443
      Height = 28
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = #1042#1099' '#1076#1077#1081#1089#1090#1074#1080#1090#1077#1083#1100#1085#1086' '#1093#1086#1090#1080#1090#1077' '#1074#1099#1081#1090#1080'?'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Yes_Button: TButton
      AlignWithMargins = True
      Left = 52
      Top = 96
      Width = 150
      Height = 49
      Margins.Left = 50
      Margins.Top = 60
      Margins.Right = 25
      Margins.Bottom = 60
      Align = alLeft
      Caption = #1044#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = Yes_ButtonClick
      ExplicitLeft = 2
      ExplicitTop = 36
      ExplicitHeight = 169
    end
    object No_Button: TButton
      AlignWithMargins = True
      Left = 251
      Top = 96
      Width = 150
      Height = 49
      Margins.Left = 25
      Margins.Top = 60
      Margins.Right = 50
      Margins.Bottom = 60
      Align = alRight
      Caption = #1053#1077#1090
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = No_ButtonClick
      ExplicitLeft = 252
    end
  end
end
